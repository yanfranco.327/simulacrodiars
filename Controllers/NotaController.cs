﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimulacroDiars.Models;


namespace SimulacroDiars.Controllers
{
    public class NotaController : Controller
    {
        private readonly NotasContext context;
        public NotaController(NotasContext context)
        {
            this.context = context;
        }
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Etiquetas = context.Etiquetas.ToList();
            ViewBag.Etiquetitas = context.EtiquetaNotas.ToList();
            return View();
        }
        [HttpGet]
        public IActionResult _Index(string search, int etiquetaId = 0)
        {
            var notas = context.Notas.Include(o => o.Etiquetas).ThenInclude(o => o.Etiqueta).ToList();
            var etiqueta = context.EtiquetaNotas.ToList();
            ViewBag.Etiquetitas = etiqueta;
            ViewBag.Etiquetas = context.Etiquetas.ToList();
            if (!String.IsNullOrEmpty(search))
            {
                notas = notas.Where(o => o.Titulo.Contains(search) || o.Cuerpo.Contains(search)).ToList();
                ViewBag.Etiquetas = context.Etiquetas.Where(p => p.Nombre.Contains(search)).ToList();
                return View(notas);
            }
            if (etiquetaId != 0)
            {
                var etiquetaSeleccionada = context.EtiquetaNotas.Where(o => o.IdEtiqueta == etiquetaId).ToList();
                var notasEtiqueta = new List<Nota>();
                foreach (var eti in etiquetaSeleccionada)
                {
                    notasEtiqueta.Add(context.Notas.Where(o => o.Id == eti.IdNota).FirstOrDefault());
                }
                notas = notasEtiqueta;

            }
            return View(notas);
        }

        [HttpGet]
        public IActionResult Detalle(int id)
        {

            var etiqueta = context.Etiquetas.ToList();
            ViewBag.Etiquetas = context.EtiquetaNotas.Include(o => o.Etiqueta).ToList();
            var nota = context.Notas.Where(o => o.Id == id).FirstOrDefault();
            return View(nota);
        }
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Etiquetas = context.Etiquetas.ToList();
            return View(new Nota());
        }
        [HttpPost]
        public IActionResult Create(Nota nota, String etiqueta)
        {
            nota.Fecha = DateTime.Now;
            List<EtiquetaNota> etic = new List<EtiquetaNota>();

            if (etiqueta == null)
                ModelState.AddModelError("etiqueta", "Ingrese por lo menos una");

            if (ModelState.IsValid)
            {
                context.Notas.Add(nota);
                context.SaveChanges();
                var etiquetas = etiqueta.Split(",");
                var todasEtiquetas = context.Etiquetas.ToList();

                List<String> nuevasEtiquetas = etiquetas.Except(todasEtiquetas.Select(o => o.Nombre)).ToList();
                if (nuevasEtiquetas.Count > 0)
                {
                    foreach (var nuevaE in nuevasEtiquetas)
                    {
                        context.Etiquetas.Add(new Etiqueta() { Nombre = nuevaE });
                        context.SaveChanges();
                    }

                }
                foreach (var item in etiquetas)
                {
                    var etiquetaExistente = context.Etiquetas.Where(o => o.Nombre == item).FirstOrDefault();
                    var etique = new EtiquetaNota();
                    etique.IdEtiqueta = etiquetaExistente.Id;
                    etique.IdNota = nota.Id;
                    etic.Add(etique);
                }
                context.EtiquetaNotas.AddRange(etic);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                Response.StatusCode = 400;
                ViewBag.Etiquetas = context.Etiquetas.ToList();
                return View(nota);
            }
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.Etiquetas = context.Etiquetas.ToList();
            var nota = context.Notas.Where(o => o.Id == id).FirstOrDefault();
            return View(nota);
        }
        [HttpPost]
        public IActionResult Edit(Nota nota, int idE, String etiqueta)
        {
            //Console.WriteLine("Probando: " + idE);
            nota.Fecha = DateTime.Now;
            List<EtiquetaNota> etic = new List<EtiquetaNota>();

            if (etiqueta.Count() == 0)
                ModelState.AddModelError("Etiqueta", "Ingrese alguna.");


            if (ModelState.IsValid)
            {

                var etiquetatita = context.EtiquetaNotas.Where(o => o.IdNota == idE).ToList();
                context.EtiquetaNotas.RemoveRange(etiquetatita);
                context.Notas.Update(nota);
                context.SaveChanges();
                var etiquetas = etiqueta.Split(",");
                var todasEtiquetas = context.Etiquetas.ToList();
                List<String> nuevasEtiquetas = etiquetas.Except(todasEtiquetas.Select(o => o.Nombre)).ToList();



                if (nuevasEtiquetas.Count > 0)
                {
                    foreach (var nuevaE in nuevasEtiquetas)
                    {
                        context.Etiquetas.Add(new Etiqueta() { Nombre = nuevaE });
                        context.SaveChanges();
                    }

                }

                foreach (var item in etiquetas)
                {
                    var etiquetaExistente = context.Etiquetas.Where(o => o.Nombre == item).FirstOrDefault();
                    var etique = new EtiquetaNota();
                    etique.IdEtiqueta = etiquetaExistente.Id;
                    etique.IdNota = nota.Id;
                    etic.Add(etique);
                }
                context.EtiquetaNotas.AddRange(etic);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                Response.StatusCode = 400;
                ViewBag.Etiquetas = context.Etiquetas.ToList();
                return View(nota);
            }
        }

        [HttpGet]
        public IActionResult Eliminar(int id)
        {
            var nota = context.Notas.Where(o => o.Id == id).FirstOrDefault();
            var etiqueta = context.EtiquetaNotas.Where(o => o.IdNota == id).ToList();
            context.Notas.Remove(nota);
            context.EtiquetaNotas.RemoveRange(etiqueta);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
