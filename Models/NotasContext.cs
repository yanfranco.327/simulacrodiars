﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimulacroDiars.Models.Maps;


namespace SimulacroDiars.Models
{
    public class NotasContext : DbContext
    {
        public DbSet<Nota> Notas { get; set; }
        public DbSet<Etiqueta> Etiquetas { get; set; }
        public DbSet<EtiquetaNota> EtiquetaNotas { get; set; }
        public NotasContext(DbContextOptions<NotasContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           // base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new NotaMaps());
            modelBuilder.ApplyConfiguration(new EtiquetaMaps());
            modelBuilder.ApplyConfiguration(new EtiquetaNotaMaps());

        }
    }
}