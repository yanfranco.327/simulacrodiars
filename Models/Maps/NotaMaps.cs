﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SimulacroDiars.Models.Maps
{
    public class NotaMaps : IEntityTypeConfiguration<Nota>
    {
        public void Configure(EntityTypeBuilder<Nota> builder)
        {
            builder.ToTable("Nota");
            builder.HasKey(o => o.Id);
            builder.HasMany(o => o.Etiquetas).WithOne().HasForeignKey(o => o.IdNota);
        }
    }
}
