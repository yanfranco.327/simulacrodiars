﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulacroDiars.Models
{
    public class Etiqueta
    {
        public int Id { set; get; }
        public string Nombre { set; get; }
    }
}
